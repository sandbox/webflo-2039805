<?php

/**
 * @file
 * Contains \Drupal\markdown\Plugin\Filter\FilterMarkdown.
 */

namespace Drupal\markdown\Plugin\Filter;

use Drupal\Core\Annotation\Translation;
use Drupal\filter\Annotation\Filter;
use Drupal\filter\Plugin\FilterBase;
use Michelf\Markdown;

/**
 * Provides a filter to convert markdown to HTML.
 *
 * @Filter(
 *   id = "filter_markdown",
 *   module = "markdown",
 *   title = @Translation("Markdown"),
 *   description = @Translation("Allows content to be submitted using Markdown, a simple plain-text syntax that is filtered into valid XHTML."),
 *   type = FILTER_TYPE_MARKUP_LANGUAGE
 * )
 */
class FilterMarkdown extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode, $cache, $cache_id) {
    return Markdown::defaultTransform($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return t('Quick Tips:<ul>
        <li>Two or more spaces at a line\'s end = Line break</li>
        <li>Double returns = Paragraph</li>
        <li>*Single asterisks* or _single underscores_ = <em>Emphasis</em></li>
        <li>**Double** or __double__ = <strong>Strong</strong></li>
        <li>This is [a link](http://the.link.example.com "The optional title text")</li>
        </ul>For complete details on the Markdown syntax, see the <a href="http://daringfireball.net/projects/markdown/syntax">Markdown documentation</a> and <a href="http://michelf.com/projects/php-markdown/extra/">Markdown Extra documentation</a> for tables, footnotes, and more.');
    }
    else {
      return t('You can use <a href="@filter_tips">Markdown syntax</a> to format and style the text. Also see <a href="@markdown_extra">Markdown Extra</a> for tables, footnotes, and more.', array('@filter_tips' => url('filter/tips'), '@markdown_extra' => 'http://michelf.com/projects/php-markdown/extra/'));
    }
  }

}
